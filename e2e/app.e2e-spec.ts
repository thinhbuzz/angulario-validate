import { AngularValidatePage } from './app.po';

describe('angular-validate App', () => {
  let page: AngularValidatePage;

  beforeEach(() => {
    page = new AngularValidatePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
