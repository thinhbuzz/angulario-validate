import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  @Input() needValidate: any;
  @Input() bodyValidateForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
  }

  addAddress() {
    this.needValidate.body.addresses.push({value: ''});
    (<FormArray>this.bodyValidateForm.controls.addresses).push(this.formBuilder.group({
      value: ['', [Validators.required, Validators.minLength(6)]]
    }));
  }
}
