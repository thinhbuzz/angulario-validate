import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  needValidate: any = {};
  validateForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.needValidate = {
      baseName: 'baseName',
      header: {
        headerBaseName: 'headerBaseName'
      },
      body: {
        bodyBaseName: 'bodyBaseName',
        addresses: []
      },
      footer: {
        footerBaseName: 'footerBaseName'
      }
    };
    this.validateForm = this.formBuilder.group({
      baseName: [this.needValidate.baseName, [Validators.required, Validators.minLength(6)]],
      header: this.formBuilder.group({
        headerBaseName: [this.needValidate.header.headerBaseName, [Validators.required, Validators.minLength(6)]]
      }),
      body: this.formBuilder.group({
        bodyBaseName: [this.needValidate.body.bodyBaseName, [Validators.required, Validators.minLength(6)]],
        addresses: this.formBuilder.array([])
      })
    });
  }

}
