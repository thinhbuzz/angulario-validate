import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidateService } from './validate.service';
import { MessageComponent } from './message/message.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MessageComponent],
  providers: [ValidateService],
  exports: [MessageComponent]
})
export class ValidateModule {
}
